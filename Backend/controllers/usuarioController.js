const Usuario = require('../models/Usuario');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.registrar = async (req,res) =>{

    let body = req.body;

    let { username, password } = body;
    let usuario = new Usuario({
        username,
        password: bcrypt.hashSync(password, 10),
    });
    usuario.save((err, usuarioDB) => {
        if (err) {
        return res.status(400).json({
            ok: false,
            err,
        });
        }
        res.json({
            ok: true,
            usuario: usuarioDB
        });
        })

}

// middleware to validate token (rutas protegidas)
exports.verifyToken = async (req, res, next) => {
    const token = req.header('bearer')
    if (!token) return res.status(401).json({ error: 'Acceso denegado' })
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET)
        req.user = verified
        next() // continuamos
    } catch (error) {
        res.status(400).json({error: 'token no es válido'})
    }
}

exports.login = async (req,res) => {
    try
    {
        let body = req.body;


        let usuario = await Usuario.findOne({username : body.username});

        if (!usuario) {
            return res.status(400).json({
              ok: false,
              err: {
                  message: "Usuario incorrecto"
              }
           })
         }

        let passwordIsValid = bcrypt.compareSync(
                req.body.password,
                usuario.password
        );
        
        if(!passwordIsValid){
            return res.status(401).send({
                accessToken: null,
                message: "Password Invalido"
              });
        }


        let token = jwt.sign({
            username : usuario.username,
            id: usuario._id
         },process.env.TOKEN_SECRET,{
         expiresIn: '48h'
        })
         
         res.json({
            ok: true,
            mensaje: "bienvenido",
            token,
        })
        


    }catch(error)
    {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

