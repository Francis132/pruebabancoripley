const mongoose = require('mongoose');

const TransferenciaSchema = mongoose.Schema({
    monto:{
        type: Number,
        require: true
    },
    fecha:{
        type: Date,
        default: Date.now()
    },
    destinatario: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Destinatario' 
    }
});

module.exports = mongoose.model('Transferencia',TransferenciaSchema);