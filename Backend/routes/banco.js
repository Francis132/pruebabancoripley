const express = require('express');
const router = express.Router();
const destinatarioController = require('../controllers/destinatarioController');
const transferenciaController = require('../controllers/transferenciaController');
const usuarioController = require('../controllers/usuarioController');


router.post('/usuarios/login', usuarioController.login);
router.post('/usuarios/registrar', usuarioController.registrar);

router.get('/destinatarios/listado',usuarioController.verifyToken, destinatarioController.obtenerDestinatarios);
router.post('/destinatarios/crear',usuarioController.verifyToken,destinatarioController.crearNuevoDestinatario);
router.get('/destinatarios/rut/:rut',usuarioController.verifyToken,destinatarioController.obtenerRut);

router.post('/transferencias/hacer',usuarioController.verifyToken,transferenciaController.hacerTransferencia);
router.get('/transferencias/listado',usuarioController.verifyToken,transferenciaController.historialTransferencias);


module.exports = router;
