import { Destinatario } from "./destinatario";

export class Transferencia{
    _id?: number;
    monto:number;
    fecha?: Date;
    destinatario: Destinatario;

    constructor(monto:number, destinatario: Destinatario){
        this.monto = monto;
        this.destinatario = destinatario;
    }
}