export class Destinatario{
    _id?: number;
    nombre: string;
    rut: string;
    correo: string;
    numTelefono: string;
    banco:  string;
    tipoCuenta: string;
    numeroCuenta: string;

    constructor(nombre: string,rut: string,correo: string,numTelefono: string,banco:string,tipoCuenta:string,numeroCuenta:string){
        this.nombre = nombre;
        this.rut = rut;
        this.correo = correo;
        this.numTelefono = numTelefono;
        this.banco = banco;
        this.tipoCuenta = tipoCuenta;
        this.numeroCuenta = numeroCuenta;        
    }
}