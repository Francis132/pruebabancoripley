import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoTransferenciaComponent } from './listado-transferencia.component';

describe('ListadoTransferenciaComponent', () => {
  let component: ListadoTransferenciaComponent;
  let fixture: ComponentFixture<ListadoTransferenciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoTransferenciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoTransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
