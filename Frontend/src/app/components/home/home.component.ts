import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Bienvenido a Banco Ripley';

  constructor( private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    const token = this.usuarioService.Token();
    if(token == null){
      this.router.navigate(['/']);
    }
  }

  //cierra la session

  logout(){
    Swal.fire({
      title: 'Su Sesion',
      text: ' Ha sido Cerrada con Exito',
      icon: 'success',
      confirmButtonText: 'Cool'        
    })
    this.usuarioService.NgDestroy();
    this.router.navigate(['/']);
  }

}
