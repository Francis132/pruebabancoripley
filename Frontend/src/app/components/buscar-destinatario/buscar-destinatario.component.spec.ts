import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarDestinatarioComponent } from './buscar-destinatario.component';

describe('BuscarDestinatarioComponent', () => {
  let component: BuscarDestinatarioComponent;
  let fixture: ComponentFixture<BuscarDestinatarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscarDestinatarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarDestinatarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
