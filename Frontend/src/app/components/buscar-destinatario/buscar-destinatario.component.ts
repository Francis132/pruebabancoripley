import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Banco } from 'src/app/models/banco';
import { Destinatario } from 'src/app/models/destinatario';
import { Transferencia } from 'src/app/models/transferencia';
import { DestinatarioService } from 'src/app/services/destinatario.service';
import { TransferenciasService } from 'src/app/services/transferencias.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-buscar-destinatario',
  templateUrl: './buscar-destinatario.component.html',
  styleUrls: ['./buscar-destinatario.component.css']
})
export class BuscarDestinatarioComponent implements OnInit {

  destinatario = Destinatario;
  transferencia: Transferencia = {
    monto: 0,
    destinatario: Object({_id: 0,})
  }
  rut = "";
  destinatarioSeleccionado: Destinatario[] =[];
  monto=0;
  id = 0;
  title = "Detalle del Destinatario";
  nombreBanco = "";
  mensajeMonto = "";
  
  constructor(private destinatarioService: DestinatarioService, private transferenciasService: TransferenciasService) {

   }
  


  ngOnInit(): void {
  }

  //busca los destinatarios existentes
  buscarDestinatario(): void{

    this.destinatarioService.buscarDestinatario(this.rut)
    .subscribe(
      data  => {
        this.destinatarioSeleccionado.pop();
        this.destinatarioSeleccionado.push(data);
        this.id = Object(data._id);
        this.nombreBanco = this.destinatarioService.bancos(data.banco,this.nombreBanco);
        console.log(this.nombreBanco);
        
      },
      error => {
        console.log(error);
      });
  }

  
  //hace la transferencia correspondientes
  hacerTransferencia(){
    const trans = {
      monto: this.monto,
      destinatario:  Object(this.id),      
    }
    console.log(this.monto);
    if(this.monto > 0){

      this.mensajeMonto = "";

    
      this.transferenciasService.crearTransferencia(trans).subscribe(res => {

        Swal.fire({
          title: 'La Transferencia!',
          text: 'Ha sido hecha con Exito',
          icon: 'success',
          confirmButtonText: 'Cool'        
        })
      })
    }else{
      this.mensajeMonto = "El monto debe ser mayor a 0";
    }
    

    
  }

  

}
