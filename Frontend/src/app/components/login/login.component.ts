import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: Usuario = {
    username: '',
    password: '',
  };
  submitted = false;

  formLogin: FormGroup;

  title = "Banco Ripley";
  logins = "Ingrese sus Datos";


  constructor( private router: Router,formBuilder: FormBuilder,private usuarioService: UsuarioService) 
  {
    this.formLogin = formBuilder.group({
      username: ['',Validators.required],
      password: ['',Validators.required]
    });
  }

  ngOnInit(): void {
  }

  Login()  
  {
    const login: Usuario = {
      username: this.usuario.username,
      password: this.usuario.password
    };

    this.usuarioService.Login(login).subscribe(res => {
      if(res.token != null){
        const token = res.token;
        sessionStorage.setItem('token',token);
        this.router.navigate(["/home"]);
      }else{
        this.router.navigate(["/"]);
      }
    });
  }

}
