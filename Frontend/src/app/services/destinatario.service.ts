import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable} from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Destinatario } from '../models/destinatario';
import { Banco } from '../models/banco';

const URL = 'http://localhost:4000/api/banco/destinatarios';
//const URL = '1';


@Injectable({
  providedIn: 'root'
})
export class DestinatarioService {

  constructor(private http: HttpClient) 
  {
  }

  crearDestinatario(destinatario: Destinatario): Observable<any>{
    return this.http.post<Destinatario>(URL+"/crear", destinatario);
  }
    

  buscarDestinatario(rut: any): 
  Observable<Destinatario> {
    console.log("servicio"+1);
    return this.http.get<Destinatario>(`${URL}/rut/`+rut);
  }

  bancos(banco: string, nombre: string):string{
    switch(banco){
      case "0000001":
          nombre = "Banco Ripley";
      break;
      case "0000002":
          nombre = "Banco Desarrollo";
      break;
      case "0000003":
          nombre = "Banco Estado";
      break;
      case "0000004":
          nombre = "Banco Chile";
      break;
      case "0000006":
          nombre = "Banco Santander";
      break;
      case "0000007":
          nombre = "Banco Edwards";
      break;
      case "0000008":
          nombre = "Banco BCIbe";
      break;
    }
    return nombre;

  }

  listadoBanco(): Observable<Banco[]> {
   
    const link = "https://bast.dev/api/banks.php";
    
    return this.http.get<Banco[]>(link );
  }
}
