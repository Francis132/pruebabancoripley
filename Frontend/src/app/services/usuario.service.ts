import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';

const URL = 'http://localhost:4000/api/banco/usuarios';

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {

  constructor(private http: HttpClient) { }

  Login(usuario:Usuario)  {
    return this.http.post<Usuario>(URL+"/login",usuario);
  }

  Token(){
    return sessionStorage.getItem('token');
  }  

  // hace que desaparesca el Token
  NgDestroy(){
    sessionStorage.removeItem('token');
  }


}
