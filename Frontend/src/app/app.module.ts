import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrearDestinatarioComponent } from './components/crear-destinatario/crear-destinatario.component';
import { BuscarDestinatarioComponent } from './components/buscar-destinatario/buscar-destinatario.component';
import { ListadoTransferenciaComponent } from './components/listado-transferencia/listado-transferencia.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './interceptors/interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    CrearDestinatarioComponent,
    BuscarDestinatarioComponent,
    ListadoTransferenciaComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule 
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS, 
    useClass: InterceptorService, 
    multi: true
  }
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
