import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CrearDestinatarioComponent } from './components/crear-destinatario/crear-destinatario.component';
import { ListadoTransferenciaComponent } from './components/listado-transferencia/listado-transferencia.component';
import { BuscarDestinatarioComponent } from './components/buscar-destinatario/buscar-destinatario.component';



const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'crearDestinatario', component: CrearDestinatarioComponent },
  { path: 'historialTransferencias', component: ListadoTransferenciaComponent },
  { path: 'buscarDestinatario', component: BuscarDestinatarioComponent}
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
