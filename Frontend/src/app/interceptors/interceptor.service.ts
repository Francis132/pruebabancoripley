import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { UsuarioService } from '../services/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService  implements HttpInterceptor{


  //esta parte busca interceptar el token de la app de node
  constructor(private usuarioService: UsuarioService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.usuarioService.Token();

    if(token){
      const clonado = req.clone({
        headers: req.headers.set("Bearer", token)
      })
      console.log(clonado);

      return next.handle(clonado);
    }
    else{
      return next.handle(req);
    }

  }
}
